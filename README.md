## YouApp
Aplicacion que consume la API DE YOUTUBE v3

## Install
npm install

## Use in development
ng serve

## Preview
ng serve
![2020-07-03_01_23_13-Window](/uploads/e700a4d94bd8294eab286cfd827a78d4/2020-07-03_01_23_13-Window.png)
![2020-07-03_01_23_44-Window](/uploads/f854f5d2fe6ddac96bf3e94ce91c7905/2020-07-03_01_23_44-Window.png)
![2020-07-03_01_24_00-Window](/uploads/641138c29e59696af5466124993d3bbf/2020-07-03_01_24_00-Window.png)


## Creator of the login_demo project
:bust_in_silhouette: **Sergio V.O**
* Twitter: [@https://twitter.com/smavo24](https://twitter.com/smavo24)
* Github: [@smavo](https://github.com/smavo)
* Gitlab: \[@smavo\] (https://gitlab.com/smavo)
* LinkedIn: [@https://www.linkedin.com/in/smavo24/](https://www.linkedin.com/in/smavo24/)





# Youapp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
