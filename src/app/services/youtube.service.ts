import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from "rxjs/operators";
import { YoutubeResponse, Video } from "../models/youtube.models";

@Injectable({
  providedIn: 'root'
})
export class YoutubeService {

  private youtubeURL = "https://www.googleapis.com/youtube/v3";
  private apkiKey = "AIzaSyCg4CfB2Ws4wkj-Lu-Hr_9iDJFKrvjTt00";
  private playList = "UUuaPTYj15JSkETGnEseaFFg";
  private nextPageToken = "";

  constructor(public http: HttpClient) { }

  getVideos() {
    const url = `${this.youtubeURL}/playlistItems`;
    const params = new HttpParams()
      .set('part', 'snippet')
      .set('maxResults', '12')
      .set('playlistId', this.playList)
      .set('key', this.apkiKey)
      .set('pageToken', this.nextPageToken);
    return this.http
      .get<YoutubeResponse>( url, { params: params} )
      .pipe(
        map((resp) => {
          this.nextPageToken = resp.nextPageToken;
          return resp.items;
        }),
        map(items => {
          return items.map(video => video.snippet)
        })
      );

  }

}
